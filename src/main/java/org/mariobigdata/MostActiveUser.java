package org.mariobigdata;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import scala.Tuple2;

public class MostActiveUser {

	static Logger log = Logger.getLogger(PasswdAnalysis.class.getName());


	public static void main(String[] args) {

		// Step 1: Create a SparkConf object
		if (args.length < 1) {
			log.fatal("Syntax Error: there must be one argument)");
			throw new RuntimeException();
		}

		// Step 2: create a SparkConf object

		SparkConf sparkConf = new SparkConf().setAppName("Trendig Topic");

		// Step 3: create a Java Spark context

		JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);

		JavaRDD<String> lines = sparkContext.textFile(args[0]);
		
		//Split all the lines of the document by :
		JavaRDD<String[]> split = lines.map(line -> line.split(("\t")));
				
	    //Get the columm where is the users.
		JavaRDD<String> users = split.map(fields -> fields[1]);
		
		//Word Count
		JavaPairRDD<String, Integer> mapusers = users.mapToPair(s -> new Tuple2<String, Integer>(s,1));
		JavaPairRDD<String, Integer> reduceusers = mapusers.reduceByKey((integer1,integer2) -> integer1 + integer2);
		Tuple2<Integer, String> sortusers = reduceusers.mapToPair(s -> new Tuple2<Integer, String>(s._2(),s._1)).sortByKey(false).first();

		System.out.println("The users who has written the amount of tweets is: " + sortusers._2() + " Total tweets: " + sortusers._1().toString());

		sparkContext.stop();
		

	}
}
