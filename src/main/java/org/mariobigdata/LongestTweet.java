package org.mariobigdata;

import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.JavaPairRDD;
import java.util.*;

import scala.Tuple2;

public class LongestTweet {
	
	static Logger log = Logger.getLogger(PasswdAnalysis.class.getName());


	public static void main(String[] args) {
		
		// Step 1: Create a SparkConf object
		if (args.length < 1) {
			log.fatal("Syntax Error: there must be one argument)");
			throw new RuntimeException();
		}

		// Step 2: create a SparkConf object

		SparkConf sparkConf = new SparkConf().setAppName("Trendig Topic");

		// Step 3: create a Java Spark context

		JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);

		JavaRDD<String> lines = sparkContext.textFile(args[0]);
		
		//Split all the lines of the document by :
		JavaRDD<String[]> split = lines.map(line -> line.split(("\t")));
		
		//Get the column where is the tweet.
		JavaRDD<String> tweet = split.map(fields -> fields[2]);
		List<String> alltweets = tweet.collect();
				
		//Get the length of the tweets and sort them
		JavaPairRDD<String, Integer> maptweet = tweet.mapToPair(s -> new Tuple2<String, Integer>(s,s.length()));
		Tuple2<Integer, String> sorttweet = maptweet.mapToPair(s -> new Tuple2<Integer, String>(s._2(),s._1)).sortByKey(false).first();
		
		//Get the information of user and date of the tweets.
		JavaRDD<String> users = split.map(fields -> fields[1]);
		List<String> allusers = users.collect();
		JavaRDD<String> dates = split.map(fields -> fields[3]);
		List<String> alldates = dates.collect();
		
		//Show the required information
		for(int i=0; i < allusers.size(); i++){
			
			if(sorttweet._2().equals(alltweets.get(i))){
				System.out.println("User: " + allusers.get(i));		
				System.out.println("Tweet: " + sorttweet._2());
				System.out.println("Num of characters: " + sorttweet._1().toString());
				System.out.println("Date: " + alldates.get(i));
				break;
			}
			
		}
		
		
		sparkContext.stop();
		
			
	}

}
