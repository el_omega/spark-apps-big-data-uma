package org.mariobigdata;


import java.util.*;

import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;


public class PasswdAnalysis {

	static Logger log = Logger.getLogger(PasswdAnalysis.class.getName());

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Step 1: Create a SparkConf object
		if (args.length < 1) {
			log.fatal("Syntax Error: there must be one argument)");
			throw new RuntimeException();
		}

		// Step 2: create a SparkConf object

		SparkConf sparkConf = new SparkConf().setAppName("Password Analysis");

		// Step 3: create a Java Spark context

		JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);

		JavaRDD<String> lines = sparkContext.textFile(args[0]);
		
		//Split all the lines of the document by :
		JavaRDD<String[]> split = lines.map(line -> line.split((":")));

		//Map the first columm to get the users, then count
		long numusers = split.map(fields -> fields[0]).count();

		System.out.println("The numbers of user accounts is: " + numusers);

		//Get a JavaRDD and List for each columm (users, UID, GUID)
		JavaRDD<String> users = split.map(fields -> fields[0]);
		JavaRDD<String> UID = split.map(fields -> fields[2]);
		JavaRDD<String> GUID = split.map(fields -> fields[3]);

		List<String> users1 = users.collect();
		List<String> UID1 = UID.collect();
		List<String> GUID1 = GUID.collect();
		

		//Sort the users by his first word, then take the five first and show the information.	
		List<String> sortuser = users.sortBy(s-> s.charAt(0), true, 1).take(5);
		
		for (int j = 0; j <= sortuser.size()-1; j++) {
			for(int i= 0; i<= users1.size()-1; i++){
				if(sortuser.get(j).equals(users1.get(i))){
					System.out.println("User name: " + users1.get(i)
							+ " UID: " + UID1.get(i) + " GUID: "
							+ GUID1.get(i));
				}
			}
		}
		
		
		//Map the last columm, where is the information /bin/bash, then filter this and count how many times appear.
		long binbas = split.map(fields -> fields[6])
				.filter(fields -> fields.contains("/bin/bash")).count();

		System.out.println("The numbers of user having bash as command when login is : " + binbas);

		sparkContext.stop();

	}

}
