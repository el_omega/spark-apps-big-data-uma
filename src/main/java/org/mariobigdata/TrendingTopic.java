package org.mariobigdata;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.*;

import scala.Tuple2;

public class TrendingTopic {

	static Logger log = Logger.getLogger(PasswdAnalysis.class.getName());

	
	public static void main(String[] args) {
		// Step 1: Create a SparkConf object
		if (args.length < 1) {
			log.fatal("Syntax Error: there must be one argument)");
			throw new RuntimeException();
		}

		// Step 2: create a SparkConf object

		SparkConf sparkConf = new SparkConf().setAppName("Trendig Topic");

		// Step 3: create a Java Spark context

		JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);

		JavaRDD<String> lines = sparkContext.textFile(args[0]);
		
		//Split all the lines of the document by :
		JavaRDD<String[]> split = lines.map(line -> line.split(("\t")));
		
		//Get the columm where is the tweet.
		JavaRDD<String> tweet = split.map(fields -> fields[2]);
		
		//Delete some common words
		JavaRDD<String> quitword = tweet.map(line -> line.replaceAll("\\bRT\\b",""));
		JavaRDD<String> quitword1 = quitword.map(line -> line.replaceAll("\\bto\\b",""));
		JavaRDD<String> quitword2 = quitword1.map(line -> line.replaceAll("\\bthe\\b",""));
		JavaRDD<String> quitword3 = quitword2.map(line -> line.replaceAll("\\bat\\b",""));
		JavaRDD<String> quitword4 = quitword3.map(line -> line.replaceAll("\\bwith\\b",""));
		JavaRDD<String> quitword5 = quitword4.map(line -> line.replaceAll("\\bin\\b",""));
		JavaRDD<String> quitword6 = quitword5.map(line -> line.replaceAll("\\ba\\b",""));
		JavaRDD<String> quitword7 = quitword6.map(line -> line.replaceAll("\\b-\\b",""));
		JavaRDD<String> quitword8 = quitword7.map(line -> line.replaceAll("\\bfor\\b",""));
		JavaRDD<String> quitword9 = quitword8.map(line -> line.replaceAll("\\bof\\b",""));
		

		
		//Split the the words of each tweet
		JavaRDD<String> splittweet = quitword9.flatMap(line -> Arrays.asList(line.split((" "))).iterator());
		
		
		//Word Count
		JavaPairRDD<String, Integer> splitwords = splittweet.mapToPair(s -> new Tuple2<String, Integer>(s,1));
		JavaPairRDD<String, Integer> sortwords = splitwords.reduceByKey((integer1,integer2) -> integer1 + integer2);
		List<Tuple2<Integer, String>> words = sortwords.mapToPair(s -> new Tuple2<Integer, String>(s._2(),s._1)).sortByKey(false).take(10);

		//Show the results
		for (Tuple2<?,?> tuple : words) {
		      System.out.println(tuple._2() + ": " + tuple._1());
		    }
		
		sparkContext.stop();
		
		
		
	}

}
